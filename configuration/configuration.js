'use strict';

class Configuration {

    constructor() {
        this.contentFilePath = null;
        this.serverPort = null;
        this.redis = {
            host: null,
            port: null
        };
    }
}

module.exports = Configuration;