'use strict';
const configuration = require('config'); // loads configuration from folder ../configuration
const logFactory = require('../loggers/loggerFactory');
const Configuration = require('./configuration');


class ConfigurationLoader {

    constructor(logger) {
        this._configInternal = null;
        this._logger = logger;
    }

    getConfig() {

        if (this._configInternal === null) {

            this._configInternal = new Configuration();

            this._configInternal.serverPort = this._getValueOrNull("app.port") || 8080;
            this._configInternal.redis.host = this._getValueOrNull("redis.host") || "pub-redis-13863.eu-central-1-1.1.ec2.redislabs.com";
            this._configInternal.redis.port = this._getValueOrNull("redis.port") || 13863;
            this._configInternal.contentFilePath = this._getValueOrNull("app.contentFilePath") || "incomingData/data.txt";

            this._logger.info(`Loaded configuration file [ ${configuration.util.getEnv("NODE_ENV")}.json ] from folder [ ${configuration.util.getEnv("NODE_CONFIG_DIR")} ]`);
        }

        return this._configInternal;
    }

    _getValueOrNull(configKey) {
        if (configuration.has(configKey)) {

            return configuration.get(configKey);
        }

        return null;
    }
}

module.exports.appConfigLoaderInstance = new ConfigurationLoader(logFactory.logFactoryInstance.getLogger());
