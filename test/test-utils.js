const assert = require('assert');
const utils = require('../util/utils');

describe('utils', function () {

    describe('isNumeric(n)', function () {

        it('should return true for number six', function () {
            assert.equal(utils.utilsInstance.isNumeric(6), true, "number 6 is numeric");
        });

        it('should return false for "a" character', function () {
            assert.equal(utils.utilsInstance.isNumeric("a"), false, "'a' is not numeric");
        });

        it('should return false for null', function () {
            assert.equal(utils.utilsInstance.isNumeric(null), false, "'a' is not numeric");
        });

        it('should return false for undefined', function () {
            assert.equal(utils.utilsInstance.isNumeric(undefined), false, "'a' is not numeric");
        });
    });


    describe('findPropertyValue(obj, propertyName)', function () {

        it('should find property "count"', function () {
            assert.equal(utils.utilsInstance.findPropertyValue({"count": 6}, "count"), 6, "expects found value = 6");
        });

        it('should not find "count" property', function () {

            assert.equal(utils.utilsInstance.findPropertyValue({"test": 0}, "count"), null, "there is no count property in input object")
        });

        it('should not find "count" property because of empty input', function () {

            assert.equal(utils.utilsInstance.findPropertyValue(null, "count"), null, "input object is null");
        });

        it('should not find "count" because property to find is null', function () {
            assert.equal(utils.utilsInstance.findPropertyValue({"count": 5}, null), null, "searched property name is null");
        });

        it('should not find result, because of input data are empty', function () {
            assert.equal(utils.utilsInstance.findPropertyValue(null, null), null, "inputs are null");
        });

    });
});