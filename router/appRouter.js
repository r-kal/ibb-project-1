const express = require('express');
const router = express.Router();
const responseTime = require('response-time');
const manager = require('../BL/trackManager').trackManagerInstance;
const HttpStatus = require('http-status-codes');
const ex = require('../exceptions/validationException');
const logger = require('../loggers/loggerFactory').logFactoryInstance.getLogger();

// hook 3rd party middleware function to measure response time
router.use(responseTime(function (req, res, time) {
    logger.trace(`Request to ${req.path} takes [ ${time.toFixed(3)} ms]`);
}));

router.post('/track', function (req, res, next) {

    try {

        manager.saveTrackAsync(req.body)
            .then(() => {

                res.status(HttpStatus.OK);
                res.json(
                    {
                        state: 'success'
                    });
            })
            .catch(err => {

                if (err instanceof ex.ValidationException) {
                    res.status(HttpStatus.BAD_REQUEST);
                    res.json({
                        validationMessage: err.message
                    });

                    return;
                }

                next(err);
            });
    } catch (error) {
        next(error);
    }
});

router.get('/count', function (req, res, next) {

    try {
        manager.getCountAsync()
            .then(c => {
                res.status(HttpStatus.OK);
                res.json({
                    count: c
                });
            })
            .error(err => {
                next(err);
            })

    } catch (err) {
        next(err);
    }
});

module.exports = router;