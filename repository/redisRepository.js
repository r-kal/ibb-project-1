'use strict';

const redisConnector = require('../storage/redisConnector');

class RedisRepository {

    static get countKey() {
        return "count";
    }

    constructor(appConfig, logger) {
        this._connector = new redisConnector.RedisConnector(appConfig, logger);
    }

    saveTrackAsync(count) {

        return this._connector.connection.getAsync(RedisRepository.countKey)
            .then((res) => {

                let increased = null;
                if (res) {
                    increased = parseInt(res) + count;
                } else {
                    increased = count;
                }

                return this._connector.connection.setAsync(RedisRepository.countKey, increased);
            });
    }

    getCountAsync() {
        return this._connector.connection.getAsync(RedisRepository.countKey)
            .then(count => {
                return parseFloat(count);
            });
    }

    dispose() {
        this._connector.dispose();
    }

}

module.exports.RedisRepository = RedisRepository;