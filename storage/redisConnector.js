'use strict';

const redis = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);

class RedisConnector {

    constructor(appConfig, logger) {

        this._logger = logger;

        this.connection = redis.createClient({
            host: appConfig.redis.host,
            port: appConfig.redis.port
        });

        this.connection.on("connect", () => {
            this._logger.info("Redis connector connected");
        });

        this.connection.on("ready", () => {
            this._logger.info("Redis connector ready");
        });

        this.connection.on("error", (err) => {
            throw err;
        });
    }

    dispose() {
        if (this.connection) {
            this.connection.quit();
            this._logger.info('Redis connector closed connection successfuly');
        }

        this._logger.info('Redis connector sucesfully disposed');
    }

}

module.exports.RedisConnector = RedisConnector;