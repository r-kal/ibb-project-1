const assert = require('assert');
const v = require('../validation/validator');
const ex = require('../exceptions/validationException');

describe('validator', function () {

    describe('validateCount(input)', function () {

        it('non empty value', function () {
            assert.ok(v.validatorInstance.validateCount(5), "testing value is non empty");
        });

        it('empty value', function () {
            try {
                v.validatorInstance.validateCount(null);
                assert.ok(false);
            } catch (err) {
                assert.ok(err instanceof ex.ValidationException, "error is ValidatioException");
            }
        });

        it('null value throws validation exception', function () {

            try {
                v.validatorInstance.validateCount("a");
                assert.ok(false);
            } catch (err) {
                assert.ok(err instanceof ex.ValidationException, "error is ValidatioException");
            }
        });
    });
});