const assert = require('assert');
const request = require('request');

describe('router', function () {

    describe('/count route', function () {

        it('should return ok', function (done) {
            request.get('http://localhost:8080/count', function (error, response, body) {

                assert.equal(response.statusCode, 200, "status code is 200");
                done();
            });
        });

        it('/count66 should return not found', function (done) {
            request.get('http://localhost:8080/count66', function (error, response, body) {

                assert.equal(response.statusCode, 404, "status code is 404");
                done();
            });
        });

        it('POST route should return not found', function (done) {
            request.post('http://localhost:8080/count',
                {
                    body: "test body string"
                }, function (error, response, body) {

                    assert.equal(response.statusCode, 404, "status code is 404");
                    done();
                });
        });
    });


    describe('/track', function () {

        it('should reject GET request', function (done) {
            request.get('http://localhost:8080/track', function (error, response, body) {

                assert.equal(response.statusCode, 404, "status code is 404");
                done();
            });
        });


        it('should save request body', function (done) {
            request.post('http://localhost:8080/track',
                {
                    body: {count: 1},
                    json: true
                },
                function (error, response, body) {

                    assert.equal(response.statusCode, 200, "status code is 200 OK");
                    done();
                });
        });

    });
});

