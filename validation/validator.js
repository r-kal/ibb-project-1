'use strict';

const u = require('../util/utils');
const ex = require('../exceptions/validationException');

class AppValidator {

    validateCount(input) {

        if (!input || !u.utilsInstance.isNumeric(input))
            throw new ex.ValidationException("Invalid input data, count property has to be number");


        return true;
    }

}

module.exports.validatorInstance = new AppValidator();