'use strict';

const globalErrHandler = require('../handler/globalErrorHandler').globalErrorHandlerInstance;
const HttpStatus = require('http-status-codes');

function errorHandler(err, req, res, next) {
    globalErrHandler.handleAsync(err)
        .then(() => {

            // do something like email to someone
            // or send some kind of notification message somewhere
            // etc.
            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
            res.json({error: "Internal server error"});

        }).catch(err => {

        // fatal error occurs when handling error
        res.status(HttpStatus.INTERNAL_SERVER_ERROR);
        res.json({error: "Internal server error"});
    });
}

module.exports = errorHandler;