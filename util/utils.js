'use strict';

class Utils {

    findPropertyValue(obj, propertyName)
    {
        if(!obj || !propertyName)
            return null;


        for(var key in obj)
        {
            if(typeof obj[key] === "object")
            {
                return this.findPropertyValue(obj[key], propertyName);
            }

            if(key === propertyName)
            {
                return obj[key];
            }
        }

        return null;
    }

    isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
}

module.exports.utilsInstance = new Utils();