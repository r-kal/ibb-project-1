class ValidationException extends Error {

    constructor(msg, id) {
        super(msg, id);
    }
}

module.exports.ValidationException = ValidationException;