'use strict';

const logger = require('../loggers/loggerFactory').logFactoryInstance.getLogger();

class GlobalErrorHandler {

    constructor(logger) {
        this._logger = logger;
    }

    handleAsync(error) {

        return new Promise((resolve, reject) => {
            this._logger.error("[ global error handler ] " + error);
            resolve();
        });
    }
}

module.exports.globalErrorHandlerInstance = new GlobalErrorHandler(logger);