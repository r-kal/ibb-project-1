const assert = require('assert');
const ve = require('../exceptions/validationException');

const bl = require('../BL/trackManager');

describe('manager', function () {

    describe('getCountAsync()', function () {

        it('should return count greater than 0', function () {

            return bl.trackManagerInstance.getCountAsync().then(count => {
                assert.equal(count >= 0, true, 'Count is greater than 0');
            });
        });
    });

    describe('saveTrackAsync(obj)', function () {

        it('should throw an exception for null', function () {

            return bl.trackManagerInstance.saveTrackAsync(null)
                .catch(err => {

                    assert.ok(err, "exception during save track");
                    assert.ok(err.message, "exception message is empty");
                    assert.equal(err.message, "Request body cannot be null", "exception message is not ok");
                });
        });


        it('should fail on validation exception', function () {

            return bl.trackManagerInstance.saveTrackAsync({count: "a"})
                .catch(err => {
                    assert.ok(err, "exception wasn't thrown");
                    assert.ok(err.message, "exception message is empty");
                    assert.ok(err instanceof ve.ValidationException, "ValidationException is expected");
                });
        });

        it('should successfuly save count', function () {

            let beforeSave = null;

            return bl.trackManagerInstance.getCountAsync()
                .then(count => {
                    beforeSave = count;

                    return bl.trackManagerInstance.saveTrackAsync({count: 1});
                })
                .then(() => {
                    return bl.trackManagerInstance.getCountAsync();
                })
                .then(afterSaveCount => {
                    assert.equal(beforeSave, afterSaveCount - 1, "saving count does not work properly");
                });
        });
    });
});