'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const router = require('./router/appRouter');
const errHandlerMiddleware = require('./middleware/errorHandler');
const config = require('./configuration/configurationLoader');
const logging = require('./loggers/loggerFactory');
const exitHandler = require('./handler/exitHandler').exitHandlerInstance;
const globalErrHandler = require('./handler/globalErrorHandler').globalErrorHandlerInstance;

// process handlers
process.stdin.resume();

process.on('SIGINT', () => {
   _handle(null, 0);
});

process.on('uncaughtException', err => {
    _handle(err, -1);
});

process.on('unhandledRejection', err => {
    globalErrHandler.handleAsync(err);
});

var app = express();

// we support only json format in request body
app.use(bodyParser.json());

// setup routing
app.use('/', router);

// add custom error globalErrorHandlerInstance
app.use(errHandlerMiddleware);

// configuration config
let appConfig = config.appConfigLoaderInstance.getConfig();

// get default logger
let logger = logging.logFactoryInstance.getLogger();

// fire up server/configuration
app.listen(appConfig.serverPort, function () {
    logger.info(`Server started on port ${appConfig.serverPort}`);
});

// private methods

function _handle(err, exitCode) {
    exitHandler.handleError(err);

    logging.logFactoryInstance.getLogger().shutdown(() => {
        process.exit(exitCode);
    });
}
