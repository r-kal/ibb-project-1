'use strict';

const manager = require('../BL/trackManager').trackManagerInstance;
const logger = require('../loggers/loggerFactory').logFactoryInstance.getLogger();

class ExitHandler {

    constructor(logger, manager) {
        this._logger = logger;
        this._manager = manager;
    }

    handleError(err) {

        if (err) {
            this._logger.error("[uncaught exception handler] " + err);
        }

        this._manager.dispose();
    }
}

module.exports.exitHandlerInstance = new ExitHandler(logger, manager)