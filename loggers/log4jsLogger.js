'use strict';

const log4js = require('log4js');

log4js.configure('config/log-config.' + (process.env.NODE_ENV || 'development') + '.json');
const logger = log4js.getLogger();
logger.level = 'trace';

module.exports.log4jsLoggerInstance = logger;