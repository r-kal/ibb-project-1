'use strict';

const log4js = require('./log4jsLogger');
const log4jsModule = require('log4js');

class LoggerFactory {

    constructor() {
        this._logger = null;// of type LoggerWrapper
    }

    getLogger() {

        if (this._logger === null) {

            log4js.log4jsLoggerInstance.shutdown = (cb) => {
                log4jsModule.shutdown(cb);
            };

            this._logger = new LoggerWrapper(log4js.log4jsLoggerInstance);
        }

        return this._logger;
    }
}

class LoggerWrapper {

    /*
        LOG LEVELS

        trace();
        debug();
        info();
        warn();
        error();
        fatal();
     */

    constructor(logger) {
        this._logger = logger;
    }

    trace(obj) {
        this._logger.trace(obj);
    }

    debug(obj) {
        this._logger.debug(obj);
    }

    info(obj) {
        this._logger.info(obj);
    }

    warn(obj) {
        this._logger.warn(obj);
    }

    error(obj) {
        this._logger.error(obj);
    }

    fatal(obj) {
        this._logger.fatal(obj);
    }

    shutdown(callback) {
        this._logger.shutdown(callback);
    }
}

module.exports.logFactoryInstance = new LoggerFactory();
