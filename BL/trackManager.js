'use strict';

const logger = require('../loggers/loggerFactory').logFactoryInstance.getLogger();
const rep = require('../repository/redisRepository')
const config = require('../configuration/configurationLoader').appConfigLoaderInstance.getConfig();
const fs = require('fs');
const bluebird = require('bluebird');
const mkdirp = require('mkdirp');
const utils = require('../util/utils').utilsInstance;
const validator = require('../validation/validator').validatorInstance;
const os = require('os');

bluebird.promisifyAll(fs);
bluebird.promisifyAll(mkdirp);

class TrackManager {

    constructor(logger, repository, appConfig) {

        this._logger = logger;
        this._repository = repository;
        this._appConfig = appConfig;
    }

    saveTrackAsync(requestBody) { // requestBody as a object

        if (!requestBody)
            return Promise.reject(new Error("Request body cannot be null"));

        return this._saveContentToFileAsync(requestBody)
            .then(() => {

                // find 'count'
                let result = utils.findPropertyValue(requestBody, "count");

                // valid or exception
                validator.validateCount(result);

                let count = parseFloat(result);
                return this._repository.saveTrackAsync(count)
                    .then(res => {
                        return count;
                    })
            })
            .then(count => {
                this._logger.debug(`Successfuly saved count [ ${count} ]`);
            });
    }

    _saveContentToFileAsync(content) {

        let path = this._appConfig.contentFilePath;

        return fs.statAsync(path)
            .then((stats) => {

                return this._appendAsync(path, content);

            }).catch(err => {
                // folder does not exist ?

                let index = path.lastIndexOf("/");
                return mkdirp.mkdirpAsync(path.substring(0, index))
                    .then(() => {
                        return this._appendAsync(path, content);
                    });
            });
    }

    _appendAsync(path, content) {

        let stringified = content;

        if (typeof content === "object") {
            stringified = JSON.stringify(content);
        }

        // add newline - append does not do that for us
        stringified += os.EOL;

        return fs.appendFileAsync(path, stringified);
    }

    getCountAsync() {

        return this._repository.getCountAsync()
            .then(count => {
                this._logger.debug(`Successfuly retrieved count [ ${count} ]`);
                return count;
            });
    }

    dispose() {
        this._repository.dispose();
    }
}

module.exports.trackManagerInstance = new TrackManager(logger, new rep.RedisRepository(config, logger), config);